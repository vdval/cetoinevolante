
### CV - Mes spécialités en développement


- Markdown : un langage de description léger et facile à utiliser. Je le pratique tous les jours pour les documentations ou pour des sites internet vitrines sur GitHub.

- HTML5 - CSS3 et Bootstrap : je pratique chaque jour, mes outils sont Visual Studio Code, vi, GitHub, git, GitLab, Bitbucket, Terminal Windows, Git Bash.

- HTML5 - CSS3 - Bootstrap ou pas - Javascript pour dynamiser les sites internet : je pratique toutes les semaines et je suis en mise à jour.

- Angular à partir de la version 8 : je pratique toutes les semaines et je suis en mise à jour, mes outils sont Node.js et Angular CLI, Visual Studio Code.

- Java : je pratique toutes les semaines et je suis en mise à jour, mes outils sont IntelliJ avec JavaFX, AWT, swing. Je peux aussi coder sur Visual Studio Code.

- VBA : un vieux langage bien utile, qu'on trouve en informatique de gestion, pour la bureautique mais pas seulement. On le trouve dans l'industrie pour des bases ACCESS assez pointues. Je travaille essentiellement avec ACCESS 2010 et 2019. Je n'ai pas pratiqué depuis quelques mois.

- MongoDB et Node.js, Express.js : mise à jour en cours (back end).


- Anglais : incontournable, je parle avec un accent effroyable mais on me comprend, je le lis correctement, heureusement. J'ai déjà travaillé uniquement en anglais.
- English : one undestands me. I think. ;) I read it.

Markdown is a lightweight and easy-to-use syntax for styling your writing. It includes conventions for

```markdown
Syntax highlighted code block

# Header 1 Titre 1
## Header 2 Titre 2
### Header 3 Titre 3

- Bulleted : j'ai 33 ans d'expérience, je n'ai pas fait que du développement en continu mais j'y reviens toujours.
- List : je suis cool :)

1. Numbered : j'ai 25 ans d'expérience en développement Web, paramétrage CMS et autres sur le net. Ce que je préfère ? La technique.
2. List : mon niveau de diplôme, BAC + 4. 

**Bold** and _Italic_ and `Code` text
**Me joindre ?** _envoyez un message sur mon email d'ici en me laissant vos coordonnées_ `Je vous recontacterai, n'oubliez pas de me laisser votre Email`

[Link](url) and ![Image](src)
```


